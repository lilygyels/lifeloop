package model

import "goapp/datastore/postgres"

type Admin struct {
	FullName string
	Email    string
	Password string
}

const queryInsertAdmin = "INSERT INTO admin(fullname, email, password) VALUES($1, $2, $3);"

func (adm *Admin) Create() error {
	_, err := postgres.Db.Exec(queryInsertAdmin, adm.FullName, adm.Email, adm.Password)
	return err
}

package routes

import (
	"github.com/gorilla/mux"
	"goapp/controller"
	"log"
	"net/http"
)

func InitializeRoutes() {
	router := mux.NewRouter()

	// router.HandleFunc("/student", controller.AddStudent).Methods("POST")
	// router.HandleFunc("/student/{sid}", controller.GetStud).Methods("GET")
	// router.HandleFunc("/student/{sid}", controller.UpdateStud).Methods("PUT")
	// router.HandleFunc("/student/{sid}", controller.DeleteStud).Methods("DELETE")

	// router.HandleFunc("/students", controller.GetAllStuds)

	router.HandleFunc("/signup", controller.Signup).Methods("POST")

	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)

	log.Println("Application running on port 8080...")
	log.Fatal(http.ListenAndServe(":8080", router))
}
